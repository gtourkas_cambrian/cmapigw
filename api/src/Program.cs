using Microsoft.AspNetCore.Mvc;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Jpeg;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.ImageSharp.Drawing.Processing;
using SixLabors.Fonts;
using System.Xml.Serialization;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();

var app = builder.Build();

app.MapGet("/health", () => {
    return Results.Ok();
});

app.MapGet("/renderbyaddress/{layer}/{country}/{address}", (
    HttpContext ctx,
    string layer, 
    string country, 
    string address,
    [FromQuery(Name = "width")] int? width,
    [FromQuery(Name = "height")] int? height
) => 
{
    // validations
    var allowedlayers = new string[] { "L1", "L2" };
    if (!allowedlayers.Any(_ => _.Equals(layer, StringComparison.CurrentCultureIgnoreCase))) {
        return new Fault() { detail = "The specified layer does not exist.",
            faultCode = 404,
            faultString = "NotFound" };
    }

    var allowedCountries = new string[] { "Greece", "Holland" };
    if (!allowedCountries.Any(_ => _.Equals(country, StringComparison.CurrentCultureIgnoreCase))) {
        return new Fault() { detail = "The specified country does not exist.",
            faultCode = 404,
            faultString = "NotFound" };
    }

    // defaults
    var finalWidth = 1024;
    var finalHeight = 768;

    if (width.HasValue) {
        finalWidth = width.Value;
    }
    if (height.HasValue) {
        finalHeight = height.Value;
    }


    // create an image and return
    var collection = new FontCollection();
    var family = collection.Add("./Arial.ttf");
    var font = family.CreateFont(24);
    var image = new Image<Rgba32>(finalWidth, finalHeight);
    image.Mutate(_ => _.DrawText(
        $"Layer={layer} / Country={country} / Address={address}",
        font,
        Color.White,
        new PointF(10, 10)));
    var ms = new MemoryStream();
    image.Save(ms, new JpegEncoder());
    ms.Position = 0;
    return Results.Stream(ms, contentType: "image/jpeg");
});


app.MapControllers();

app.Run();


public sealed class Fault : IResult
{

    public string detail = "";
    public int faultCode;
    public string faultString = "";

    public Fault() {}    
    
    public async Task ExecuteAsync(HttpContext ctx)
    {
        using var msFault = new MemoryStream();
        new XmlSerializer(typeof(Fault)).Serialize(msFault, this);
        msFault.Position = 0;
        ctx.Response.StatusCode = 404;
        ctx.Response.ContentType = "application/xml";
        await msFault.CopyToAsync(ctx.Response.Body);
    }
}
