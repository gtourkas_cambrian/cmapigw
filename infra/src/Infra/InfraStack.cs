using Amazon.CDK;
using Constructs;
using Amazon.CDK.AWS.APIGateway;

namespace Infra
{
    public class InfraStack : Stack
    {
        internal InfraStack(Construct scope, string id, IStackProps props = null) : base(scope, id, props)
        {

            // ALB (import)

            // S3 Bucket

            // API Gateway
            var api = new RestApi(this, "api");

            var renderbyaddress = api.Root.AddResource("renderbyaddress");
            renderbyaddress.AddMethod("GET");


        }
    }
}
